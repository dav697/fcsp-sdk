const head = document.getElementsByTagName('head')[0]
const link = document.createElement('link')
link.rel   = 'stylesheet'
link.type  = 'text/css'
link.href  = 'https://fcspdemo.myfan.co/sdk/styles.css'
//link.href  = './sdk/styles.css';
link.media = 'all'
head.appendChild(link)


interface IItem {
    id: string
    name: string
    currency: string
    price: string

    [key: string]: string
}

class MyfanAuth {

    private url: string
    private events: string[]       = []
    private onLoadCb: () => void
    private loader: any
    private loaded: boolean
    private overlay: any
    private wrapper: any
    private iFrame: any            = null
    private xDown: number          = 0
    private lastTouchX: number     = 0
    private startTimestamp: number = 0

    constructor() {
        this.on = this.on.bind(this)
    }

    init(url: string) {

        this.url = url

        this.createOverlay()
        this.createWrapper()

        this.on('close', () => {
            this.hideElements({target: null})
        })

        return this
    }

    show(path: string) {

        this.throwIfNotInitialized()

        document.body.style.overflow = 'hidden'

        if (this.getWrapper()) this.getWrapper().style.height = window.innerHeight + 'px'

        const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)

        if (iOS) {
            document.body.style.position = 'fixed'
        }


        this.createOverlay()
        this.createWrapper()

        setTimeout(() => {
            this.getOverlay().style.display = 'block'
            this.getWrapper().style.right   = '0'
        }, 0)

        if (path) this.navigate(path)

        return this

    }

    hide() {

        this.throwIfNotInitialized()

        if (this.getWrapper()) this.getWrapper().style.right = '-' + this.getWrapper().offsetWidth + 'px'
        if (this.getOverlay()) this.getOverlay().style.display = 'none'
        document.body.style.overflow = 'visible'
        document.body.style.position = 'initial'
    }

    close() {

        this.throwIfNotInitialized()

        this.removeOverlay()
        this.removeWrapper()
        this.clearEvents()

        document.body.style.overflow = 'visible'
        document.body.style.position = 'initial'
    }

    on(action: string, callback: Function) {

        function authListener(e: any) {
            if (action === e.data.action) {
                callback(e.data)
            }
        }

        if (this.isExistsEvent(action)) return this
        window.addEventListener('message', authListener.bind(this))
        this.addEvent(action)

        return this

    }

    onLoad(cb: () => void) {

        if (this.loaded) {
            cb()
        } else {
            this.onLoadCb = cb
        }

        return this
    }

    showLoader() {

        if (!this.loaded) {
            this.createOverlay()

            setTimeout(() => {
                this.getOverlay().style.display = 'block'
            }, 0)
        }

        return this
    }

    navigate(path: string) {

        return this.sendJson({
            do: 'navigation',
            to: path,
        })
    }

    buy(item: IItem) {

        item.clientProductId = item.id
        return this.sendJson({
            do       : 'payment',
            item     : item,
            parentUrl: window.location.href,
        })
    }

    protected removeLoader() {
        this.getLoader() && this.getLoader().parentNode.removeChild(this.getLoader())
        this.loader = null
    }

    protected createLoader() {

        let p = this.getLoader()

        if (p) return p

        p                  = document.createElement('p')
        p.innerText        = 'Loading ...'
        p.style.fontSize   = '40px'
        p.style.position   = 'absolute'
        p.style.color      = 'white'
        p.style.top        = '50%'
        p.style.left       = '50%'
        p.style.marginTop  = '-50px'
        p.style.marginLeft = '-50px'
        p.style.zIndex     = '99998'

        p.setAttribute('id', 'myfan-loader')


        document.body.prepend(p)

        this.loader = p
    }

    protected whileLoading(cb: Function) {
        if (!this.loaded) {
            cb()
        }
        return this
    }

    private throwIfNotInitialized() {
        if (!this.getIFrame()) throw new Error('iframe is not initialized')
    }

    private hideElements(evt: Event | { target: any }) {
        if (evt.target === this.getIFrame()) return

        if (this.getWrapper()) this.getWrapper().style.right = '-' + this.getWrapper().offsetWidth + 'px'
        if (this.getOverlay()) this.getOverlay().style.display = 'none'
        document.body.style.overflow = 'visible'
        document.body.style.position = 'initial'
    }

    private isExistsEvent(action: string): string | undefined {
        return this.events.find(event => event === action)
    }

    private addEvent(action: string) {
        this.events.push(action)
    }

    private clearEvents() {
        this.events = []
    }

    private getLoader() {
        return this.loader
    }

    private getOverlay() {
        return this.overlay
    }

    private removeOverlay() {
        this.getOverlay().parentNode.removeChild(this.getOverlay())
        this.overlay = null
    }

    private createOverlay() {

        let div = this.getOverlay()

        if (div) return div

        div = document.createElement('div')
        div.addEventListener('click', this.hideElements.bind(this))
        div.setAttribute('id', 'myfan-overlay')


        document.body.prepend(div)

        this.overlay = div
    }

    private getWrapper() {
        return this.wrapper
    }

    private removeWrapper() {
        this.getWrapper().parentNode.removeChild(this.getWrapper())
        this.wrapper = null
    }

    private createWrapper() {

        let wrapper = this.getWrapper()

        if (wrapper) return wrapper

        wrapper = document.createElement('div')

        wrapper.addEventListener('click', this.hideElements.bind(this), false)
        wrapper.addEventListener('touchstart', this.handleTouchStart.bind(this), {passive: true})
        wrapper.addEventListener('touchend', this.handleTouchEnd.bind(this), {passive: true})
        wrapper.addEventListener('touchmove', this.handleTouchMove.bind(this), {passive: true})

        wrapper.setAttribute('id', 'myfan-login-form-wrapper')

        wrapper.style.height = window.innerHeight + 'px'


        const xDiv = document.createElement('div')
        xDiv.setAttribute('id', 'empty-div')
        wrapper.prepend(xDiv)

        document.body.before(wrapper)

        this.createIFrame()

        this.wrapper = wrapper
    }

    private getIFrame() {
        return this.iFrame
    }

    private createIFrame() {

        let iframe = this.getIFrame()

        if (iframe) return iframe

        iframe = document.createElement('iframe')

        iframe.setAttribute('id', 'myfan-login-form-iframe')
        iframe.setAttribute(
            'src', this.url,
        )

        iframe.addEventListener('load', () => {
            if (this.onLoadCb) {
                this.onLoadCb()
            }
            this.loaded = true

        }, false)

        const wrapper = document.getElementById('myfan-login-form-wrapper')

        wrapper && wrapper.append(iframe)

        this.iFrame = iframe
    }

    private sendJson(message: { [key: string]: any }) {

        if (typeof message !== 'undefined') {
            const parsel = {
                source: 'client',
                data  : message,
            }

            this.getIFrame() && this.getIFrame().contentWindow.postMessage(JSON.stringify(parsel), '*')
        }
        return this
    }

    private getTouches(evt: TouchEvent) {
        return evt.touches
    }

    private handleTouchStart(evt: TouchEvent) {

        this.getWrapper().style.transition = 'none'
        this.startTimestamp                = new Date().getTime()
        const firstTouch                   = this.getTouches(evt)[0]
        this.xDown                         = firstTouch.screenX
        this.lastTouchX                    = evt.touches[0].screenX
    }

    private handleTouchEnd(evt: TouchEvent) {
        const wrapper            = this.getWrapper()
        wrapper.style.transition = 'right 0.2s'

        if (
            -parseInt(wrapper.style.right) > wrapper.offsetWidth / 2 ||
            (new Date().getTime() - this.startTimestamp < 200 && this.lastTouchX - this.xDown > 24)) {
            this.hideElements(evt)
        } else {
            wrapper.style.right      = '0'
            wrapper.style.transition = 'right 0.5s'
        }
        this.lastTouchX = 0
    }

    private handleTouchMove(evt: TouchEvent) {

        const xScreen = (evt.touches[0].screenX - this.xDown)
        const wrapper = this.getWrapper()

        if (xScreen > 0) {
            wrapper.style.right = -xScreen + 'px'
        }

        this.lastTouchX = evt.touches[0].screenX

    }
}

